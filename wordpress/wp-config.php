<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'saimon1208');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(gS:c,KpC0Z~`7@@hZ,g(*.*dPrz5)-8Dec)LMU`yjgpWt}eNUT%UO/d(jnhL^D%');
define('SECURE_AUTH_KEY',  'PZdAy^Yj/L0wrF92(vFdW9z8K]<.0@SPX:ZkHJB3#;kj!g@xD%-J==c5nHz.QYUa');
define('LOGGED_IN_KEY',    'wN;YgJ7A#qO,X5bq6>m-jljt%zY@BMi;aM`gK(aqT($#)?RH(O@^-h>H~wj$.{Po');
define('NONCE_KEY',        '4Mw3-HFlRa_4|-K|4.* ipe a~yExSgXT&f&W4B<cOcJf!i_aP!`Id{f[21M*5IB');
define('AUTH_SALT',        '(?1QL$$dL@E1n)U|$ 5<GXH=),! I^]C^emiA2D@h}!D9fL12Tlu16TCd,WQO%Vq');
define('SECURE_AUTH_SALT', 'h3MW6Bz0v647#@Y|Se4o)!bULC<U1JrhAtVF@46kS(y3-_; +^J`1 -Z`1)txC#N');
define('LOGGED_IN_SALT',   'RABUC&<.0~Dx~S_;9,BWYW2s[{IW?n.W@}!7%Qw#ap5L:e*r2A_8&*xg;yw*vYlr');
define('NONCE_SALT',       'v2JE{nE(4-5:^L1=EA|Jv_<:UV]e%-!y$pE~3&oxaDyZs )(# Pgr|p,YZm}2!=(');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
