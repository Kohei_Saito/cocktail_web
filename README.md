# README #

## アプリ名 ##
Cocktail_Web

## 概要 ##
カクテルレシピアプリWEBバージョン

## 現状 ##
XAMPPを使用しWordpressをローカルで開発できる環境の作成

## インストール方法 ##
  - 以下よりXAMPPをインストール (v3.2.2)
  
https://www.apachefriends.org/jp/index.html

 - 解凍したXAMMPフォルダのhtdocs直下にWordpressファイルを配置

 - XAMPP ControlPanelでApacheとMySQLをStartさせる
 - ブラウザでhttp://localhost/wordpress　にアクセスするとダッシュボードにリダイレクトされます



